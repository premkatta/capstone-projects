import React from 'react';
import '../css/Login.css';

const Login =()=>{
	return(
	  <div className='login-page'>
	    <div className='login-header'>
	      <img src='https://www.lynk.co.in/Images/cp_logo_desktop.png' alt='logo'></img>
	      <a className='portal' >PORTAL</a> 
	    </div>
	    <div className='login-form'>
	      <div className='login-title'>
	        <a>SIGN IN </a><label>WITH LYNK ACCOUNT</label>
	      </div>
	      <div className='login-fields'>
	        <div className='email'>
	          <p>EMAIL / MOBILE</p>
	          <input type='text' />
	        </div>
	        <div className='password'>
	          <p>PASSWORD</p>
	          <input type='password' />
	        </div>
	        <button className='submit'>SIGN IN</button> 
	        <div className='forgot-password'><a>FORGOT PASSWORD</a></div>
	      </div>
	    </div>
	  </div>
	);
}

export default Login;