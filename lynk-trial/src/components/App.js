import React from 'react';
import Homepage from './Homepage';
import Login from './Login';

class App extends React.Component{
  render(){
    return(
      <div className='app-main'>
        <Homepage />
        <Login />
      </div>
    );
  }
}

export default App;
