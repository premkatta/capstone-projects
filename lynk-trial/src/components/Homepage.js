import React from 'react';
import '../css/Homepage.css';

const Homepage = ()=>{
	var img='';
  return(
    <div className='homepageMain'>
      <div className='header'>
        <span className='left'>
          <img className='menu' src={require('../images/menu.png')} alt='MENU'/>
          <img className='logo' src={require('../images/lynk_logo.png')} alt='LYNK'/>
        </span>
        <div className='right'>
	        <div className='track'><button>TRACK ORDER</button> </div>
	        <div className='portal'><button>LYNK PORTAL</button> </div>
        </div>
      </div>
      <div className='line' />
      <div className='homeCenter'>
        <div className='phoneDisplay'>
          <img src={require('../images/book.png')} alt='placeholder'/>
        </div>
        <div className='homeCenterData'>
          <div className='bookTruckTitle'>
            <p className='mainTitle'>Book a mini-truck in minutes</p>
            <p className='subTitle'>anywhere, anytime with our app</p>
          </div>
          <div className='bookTruckIcons'>
            <div className='singleIcon'>
              <img src={require('../images/truck.png')} alt='truck'/>
              <p>BOOK IN SECONDS</p>
            </div>
            <div className='singleIcon'>
              <img src={require('../images/truck.png')} alt='truck'/>
              <p>BOOK IN SECONDS</p>
            </div>
            <div className='singleIcon'>
              <img src={require('../images/truck.png')} alt='truck'/>
              <p>BOOK IN SECONDS</p>
            </div>
            <div className='singleIcon'>
              <img src={require('../images/truck.png')} alt='truck'/>
              <p>BOOK IN SECONDS</p>
            </div>
          </div>
          <div className='appDownload'>
            <div className='playstore'>
              <img src={require('../images/playstore.png')} alt='playstore' />
            </div>
            <div className='appstore'>
              <img src={require('../images/appstore.png')} alt='appstore' />
            </div>
          </div>
        </div>
         <div className='reviewDiv'>
           Corousel
         </div>
         <div className='servingStations'>
           <p><a href='#'>Servicing Cities</a> Chennai, Hyderabad, Mumbai</p>
         </div>
         <div className='interest'>
          <p>Own or like to invest in trucks?</p>
          <button>Earn With Us</button>
         </div>
	    </div>
      <div className='footer'>
        <div className='footerData'>
          <div className='footerLinks'>
            <p>FAQ</p>
            <p>Careers</p>
            <p>Contact Us</p>
          </div>
          <div className='copyrights'>
            <p>© 2017 Lynks Logistics Ltd </p>
            <p>Terms of use</p>
            <p>Privacy</p>
          </div>
        </div>
        <div className='footerIcons'>
          <img src={require(`../images/fb_icon.png`)} alt='facebook'/>
          <img src={require('../images/linkedin_icon.png')} alt='instagram'/>
        </div>
      </div>
	  </div>
  );
}

export default Homepage;