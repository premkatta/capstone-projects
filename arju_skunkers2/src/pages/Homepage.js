import React from 'react';
import Navbar from '../components/Navbar';

function Homepage() {
  return (
    <div className="Homepage">
		 <Navbar />
    </div>
  );
}

export default Homepage;
