import React, { Component } from 'react';

import Navbar from './navbar';
import Carousel from './carousel';
import MovieCards from './movieCards';
import FooterPanel from './footerPanel';

class Home extends Component {
  render() {
    return (
      <div className="App">   
        <Navbar/>
        <Carousel/>
        <MovieCards/>
        <FooterPanel/>
      </div>
    );
  }
}

export default Home;
