import React , {Component} from 'react';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import axios from 'axios';

import Navbar from './navbar';
import FooterPanel from './footerPanel';
import '../css/movieDetailedInfo.css';

class MovieDetailedInfo extends Component{
  constructor(props){
    super(props);
    this.state = {selectedMovieData : []}
	}
	state = {open: false};
	
  handleClickOpen = () => {
    this.setState({ open: true });
	};
	
  handleClose = () => {
    this.setState({ open: false });
	};
	
  componentDidMount(){
	console.log('this.props.match.params',this.props.match.params.movie_name);
  var movie_name= this.props.match.params.movie_name;
  console.log('movie_name',movie_name);
	  axios.get('https://myticketbooking.herokuapp.com/cinema/'+movie_name).then((data)=>{
			this.setState({selectedMovieData:data.data});
			// if(this.state.selectedMovieData){
			// 	var bgImg = this.state.selectedMovieData[0].poster_img ;
			// }
	  });
	}

	renderedTrendingStrories = ()=>{
		return this.state.selectedMovieData.map((e,index)=>{
			if(index<3){
			 return (
			 <div> 
				<span className = "card trending_pic">
					<div class = "poster ">
						<img  src = {e.trending_pic}/>
						<p>{e.trending_data}</p>
					</div>
				</span>
				<span className = "card trending_pic">
				 <div class = "poster ">
					 <img  src = {e.trending_pic}/>
					 <p>{e.trending_data}</p>
				 </div>
			  </span>
			  <span className = "card trending_pic">
					 <div class = "poster ">
					 <img  src = {e.trending_pic}/>
					 <p>{e.trending_data}</p>
					 </div>
			  </span>
			 </div>
		 )}
		})
	};
	
    render(){
      const {fullScreen} = this.props;      
        return(
          <div>
           <Navbar/>
       		 <div class = "movieDetailedInfo">
	          <div className = "trailer" >
	            <img src = {this.state.selectedMovieData.length > 0 ? this.state.selectedMovieData[0].trailer_img : "loading"}/>   
	          </div>
         		<div className = "playCircle"> 
	            <img src = {require("../images/play-button.svg")} data-toggle="modal" data-target="#exampleModalCenter"/>
            </div>
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	            <div class="modal-dialog modal-dialog-centered" role="document">
	              <div class="modal-content">
	                <div class="modal-body">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  <span aria-hidden="true">&times;</span>
		                </button>
	                <div><iframe  src={this.state.selectedMovieData.length>0?this.state.selectedMovieData[0].youtube_trailer:"loading"} frameborder="0"  autoplay="1" allowfullscreen></iframe></div>
	                </div>
	              </div>
	            </div>
            </div>
	        <span>
            <div className = "trailerPoster">
              <img src = {this.state.selectedMovieData.length > 0 ? this.state.selectedMovieData[0].poster_img : "loading"}/>
            </div>
            <div class = "movieDetails">
                <p className = "mTitle">{this.state.selectedMovieData.length > 0 ? this.state.selectedMovieData[0].title : "loading"}</p>
                <p>{this.state.selectedMovieData.length > 0 ? this.state.selectedMovieData[0].language : "loading"}</p>
                <a className ="buttonMode">
                  <span><a>COMEDY</a></span>
                  <span><a>FANTASY</a></span>
                  <span><a>THRILLER</a></span>
                </a>
                <div className = "mDate">
                  <span><i class="far fa-calendar-alt"></i>{this.state.selectedMovieData.length > 0 ? this.state.selectedMovieData[0].releaseDate: "loading"}</span>
                  <span><i class="far fa-clock"></i>{this.state.selectedMovieData.length > 0 ? this.state.selectedMovieData[0].duration : "loading"}</span>
                </div>
            </div>
            <div className ="socialIcons2">
              <a><img src = {require("../images/facebook.svg")}/></a>
              <a><img src = {require("../images/twitter.svg")}/></a>
              <a><img src = {require("../images/pinterest.svg")}/></a>
              <a><img src = {require("../images/google-plus.svg")}/></a>
            </div>
	        </span>
          <div className = "infopanelMDI">
            <div className = "voteRating">
              <div className="ratingMDI">
                <span>
                  <img src = "https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678087-heart-512.png" alt ="like"/>
                  <a>75%</a>
                </span>
                  <div className ="voteCount">186,314 votes</div>
              </div>
              <div className="criticMDI">
                <span>
                  <a>3.5</a>
                  <img src = {require("../images/star-16.ico")}/>
                  <img src = {require("../images/star-16.ico")}/>
                  <img src = {require("../images/star-16.ico")}/>
                </span>
                  <div className ="voteCount">CRITICS RATING</div>
              </div>
              <div className="criticMDI">
                <span>
                  <a>4.1</a>
                  <img src = {require("../images/star-16.ico")}/>
                  <img src = {require("../images/star-16.ico")}/>
                  <img src = {require("../images/star-16.ico")}/>
                  <img src = {require("../images/star-16.ico")}/>
                </span>
                  <div className ="voteCount">USERS RATING</div>
              </div>
              <span className="bookNow">
	              <NavLink to ={`/movies/${this.state.selectedMovieData.length > 0 ? this.state.selectedMovieData[0].title : "loading"}/theatres/`} style={{textDecoration: 'none'}} activeStyle={{color: 'initial', textDecoration: 'none'}}>
								  <button type="button" class="btn btn-primary" >
										<a>BOOK TICKETS</a>
									</button>
								</NavLink>
              </span>
            </div>
            <div className = "mainCardsDiv infoMDI">
              <div class = "divCardTitle trendingstories">
               <a>TRENDING STORIES</a>
               <p>view all</p>
              </div>
              <div className = "trendingStories">
                {this.renderedTrendingStrories()}
              </div>   
              <div className = "synopsis">
                <a>SYNOPSIS</a>
                <p>A grave threat to humanity looms large in the form of a mysterious birdman, who will destroy anything that stands in the way of his war against technology. It is now up to Dr. Vaseegaran to revive his most ingenious creation - the humanoid Chitti - to stop the nightmarish rampage of the birdman.</p>
              </div> 
            </div>
          </div>
          <div className = "privacyPanel">
            <div className = "privacy">
              <p>Privacy Note</p>
              <p>By using www.bookmyshow.com(our website), you are fully accepting the Privacy Policy available at<a href = "https://bookmyshow.com/privacy" > https://bookmyshow.com/privacy </a>  governing your access to Bookmyshow and provision of services by Bookmyshow to you. If you do not accept terms mentioned in the <a href= "https://in.bookmyshow.com/privacy">Privacy Policy</a>, you must not share any of your personal information and immediately exit Bookmyshow.</p>
            </div>  
          </div>
        </div>
      <FooterPanel/>
    </div>
  )}
}

MovieDetailedInfo.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

export default MovieDetailedInfo;