import React , {Component} from 'react';
import '../css/movieCards.css';
import axios from 'axios';
import Navbar from './navbar';
import {NavLink} from 'react-router-dom';
import FooterPanel from './footerPanel';

class MovieAll extends Component{
  constructor(props){
    super(props);
    this.state = {abc : []}
  }
  componentDidMount(){
    // debugger
    axios.get('https://myticketbooking.herokuapp.com/BmsData').then((data)=>{
      // console.log(this.state.abc.length);
      this.setState({abc:data.data});
      console.log(this.state.abc);
    })
  }
	render(){
	  // {
	  //    var icon  = this.state.abc.length > 0 ? this.state.abc[0].poster_img : "loading";
	  // }
	  return(
	    <div>
	      <Navbar/>
	      <div className = "movieCardsPanelAll" >
	      <div className = "mainCardsDiv">
	          <div class = "divCardTitle">
	           <a>Movies</a>
	          </div>
	          <div className = "totalCards">
	          {
	            this.state.abc.map((e)=>{
	               return ( 
								 <div className = "card">
	                <NavLink to = {"/movies/"+e.title} style={{color: 'black', textDecoration: 'none'}} activeStyle={{color: 'black', textDecoration: 'none'}}>
	                  <div class = "poster">
	                    <img  src = {e.poster_img}/>
	                  </div>
	                  <div className = "info">                                 
	                    <div className = "rating">
	                      <div className = "icon"><img src = {e.heartIcon}/><a>{e.rating}</a></div>
	                      {/* <div className = "percentage"></div> */}
	                    </div>
	                    <div className = "posterTitle">
	                      <div className = "card-title">{e.title}</div>
	                      <div className = "card-tag">
	                       <span>UA | Action | {e.language}</span>
	                      </div>
	                    </div>
	                  </div>
	                </NavLink>
								</div>
								);
	            })
	          }
	          </div>      
	      </div>
	     </div>
	     <FooterPanel/>
	    </div>
	  )}
}

export default MovieAll;