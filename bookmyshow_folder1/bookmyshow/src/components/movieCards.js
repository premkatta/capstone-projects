import React, { Component } from "react"
import { NavLink } from "react-router-dom"
import axios from "axios"

import "../css/movieCards.css"

class MovieCards extends Component {
  constructor(props) {
    super(props)
    this.state = { movieData: [] }
  }
  componentDidMount() {
    // debugger
    axios
      .get("https://myticketbooking.herokuapp.com/BmsData")
      .then((data) => {
        this.setState({ movieData: data.data })
      })
      .catch((error) => console.log(error, "BmsData error"))
  }

  renderedMovieNames = () => {
    return this.state.movieData.map((e) => {
      return (
        <div className='movieName'>
          <a>{e.title}</a>
          <div className='category'>Movies</div>
        </div>
      )
    })
  }

  renderedTotalCards = () => {
    return this.state.movieData.map((e, index) => {
      if (index < 10) {
        return (
          <div className='card'>
            <NavLink
              to={"/movies/" + e.title}
              style={{ color: "black", textDecoration: "none" }}
              activeStyle={{ color: "black", textDecoration: "none" }}
            >
              <div class='poster'>
                <img src={e.poster_img} />
              </div>
              <div className='info'>
                <div className='rating'>
                  <div className='icon'>
                    <img src={e.heartIcon} />
                    <a>{e.rating}</a>
                  </div>
                  {/* <div className = "percentage"></div> */}
                </div>
                <div className='posterTitle'>
                  <div className='card-title'>{e.title}</div>
                  <div className='card-tag'>
                    <span>UA | Action | {e.language}</span>
                  </div>
                </div>
              </div>
            </NavLink>
          </div>
        )
      }
    })
  }

  render() {
    // {
    //   var icon  = this.state.movieData.length > 0 ? this.state.movieData[0].poster_img : "loading";
    // }
    return (
      <div className='movieCardsPanel'>
        <div className='trending'>
          <div className='title'>Trending Searches</div>
          <div className='widgetList'>{this.renderedMovieNames()}</div>
        </div>
        <div className='mainCardsDiv'>
          <div class='divCardTitle'>
            <a>Movies</a>
            <NavLink to='/movies'>
              <p>view all</p>
            </NavLink>
          </div>
          <div className='totalCards'>{this.renderedTotalCards()}</div>
        </div>
        <div className='privacy'>
          <p>Privacy Note</p>
          <p>
            By using www.bookmyshow.com(our website), you are fully accepting the Privacy Policy
            available at
            <a href='https://bookmyshow.com/privacy'> https://bookmyshow.com/privacy </a> governing
            your access to Bookmyshow and provision of services by Bookmyshow to you. If you do not
            accept terms mentioned in the{" "}
            <a href='https://in.bookmyshow.com/privacy'>Privacy Policy</a>, you must not share any
            of your personal information and immediately exit Bookmyshow.
          </p>
        </div>
      </div>
    )
  }
}

export default MovieCards
