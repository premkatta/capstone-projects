import React , {Component} from 'react';
import {NavLink} from 'react-router-dom';
import axios from 'axios';
import '../css/quickbook.css';

let movies =["Aravinda sametha","Taxiwaala","2.0","Sarkar","Amar Akbar Antony","Savyasachi","24 Kisses"];

class Quickbook extends Component{
    constructor(props){
      super(props);
      this.state = {abc : []}
    }
    componentDidMount(){
        axios.get('https://myticketbookimg.herokuapp.com/BmsData').then((data)=>{
          // console.log(this.state.abc.length);
          this.setState({abc:data.data, movies:data.data});
          // console.log(this.state.abc.length);
        })   
        {
        function autocomplete(inp, arr) {
          /*the autocomplete function takes two arguments,
          the text field element and an array of possible autocompleted values:*/
          var currentFocus;
          /*execute a function when someone writes in the text field:*/
          inp.addEventListener("input", function(e) {
              var a, b, i, val = this.value;
              /*close any already open lists of autocompleted values*/
              closeAllLists();
              if (!val) { return false;}
              currentFocus = -1;
              /*create a DIV element that will contain the items (values):*/
              a = document.createElement("DIV");
              a.setAttribute("id", this.id + "autocomplete-list");
              a.setAttribute("class", "autocomplete-items my-autocomplete-items ");
              /*append the DIV element as a child of the autocomplete container:*/
              this.parentNode.appendChild(a);
              /*for each item in the array...*/
              for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                  /*create a DIV element for each matching element:*/
                  b = document.createElement("DIV");
                  /*make the matching letters bold:*/
                  b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                  b.innerHTML += arr[i].substr(val.length);
                  /*insert a input field that will hold the current array item's value:*/
                  b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                  /*execute a function when someone clicks on the item value (DIV element):*/
                  b.addEventListener("click", function(e) {
                      /*insert the value for the autocomplete text field:*/
                      inp.value = this.getElementsByTagName("input")[0].value;
                      /*close the list of autocompleted values,
                      (or any other open lists of autocompleted values:*/
                      closeAllLists();
                  });
                  a.appendChild(b);
                }
              }
          });
          /*execute a function presses a key on the keyboard:*/
          inp.addEventListener("keydown", function(e) {
              var x = document.getElementById(this.id + "autocomplete-list");
              if (x) x = x.getElementsByTagName("div");
              if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
              } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
              } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                  /*and simulate a click on the "active" item:*/
                  if (x) x[currentFocus].click();
                }
              }
          });
          function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
          }
          function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
              x[i].classList.remove("autocomplete-active");
            }
          }
          function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
              if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
              }
            }
          }
          /*execute a function when someone clicks in the document:*/
          document.addEventListener("click", function (e) {
              closeAllLists(e.target);
          });
        }
        autocomplete(document.getElementById("myInput"), movies);
        /*An array containing all the country names in the world:*/
        }
        /*initiate the autocomplete function on the "myInput" element, and pass along the movies array as possible autocomplete values:*/
        }

    render(){
	    return(
	      <div className = "quickbook">
	        <div className = "searchPanel">
	          <div>
	           <NavLink to ="/"> <img src = {require("../images/left-arrow-chevron.svg")}/> </NavLink>
	          </div>
	          <div className = "cross">
	          <NavLink to ="/"> <img src = {require("../images/cross.svg")}/></NavLink>
	          </div>
	          <span class = "__icon search-icon">
	            <form autocomplete="off" action="/action_page.php">
	              <div class="autocomplete">
	                <input id="myInput" class ="search_border2" type="text" name="myCountry" placeholder="Search for Movies, Events, Plays  and Sports"/>
	              </div>
	            </form>
	            <div className = "pref" >Preferred Cinemas: Carnival: Ameerpet, AB Miniplex: Shivranjini Cross Road, Satellite, PVR Forum Sujana Mall: Kukatpally, Hyderabad</div>
	          </span>
	        </div>
	        <div className = "movieListPanel">
	          <div className = "header">
	              <span className = "changer">
	                <span><p>Movies</p></span>
	                <span><p>Cinemas</p></span>
	              </span>
	              <span className = "filter">
	                <span>Filter</span>
	                <a className ="buttonMode">
	                <span><a>Telugu</a></span>
	                <span><a>Hindi</a></span>
	                <span><a>Tamil</a></span>
	                <span><a>English</a></span>
	                </a>
	              </span>
	              <div className = "row movieList ">
	                <div className = "col-md-3">
	                  <div className = "movieLanguage"><ul> TELUGU</ul></div>
	                  <div className= "movies">
	                    <ul>
	                      <li><a>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></a></li>
	                      <li><a>Sarkar (UA) <a></a></a></li>
	                      <li><a>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></a></li>
	                      <li><a>Taxiwala (UA) <a></a></a></li>
	                      <li><a>Savyasachi (A) <a>NEW</a></a></li>
	                      <li><a>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></a></li>
	                    </ul>
	                  </div> 
	                </div>
	                <div className = "col-md-3">
	                  <div className = "movieLanguage"><ul> HINDI</ul></div>
	                  <div className= "movies">
	                    <ul>
	                    <li>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></li>
	                      <li>24 kisses  (A)<a>NEW</a></li>
	                      <li>Sarkar (UA) <a></a></li>
	                      <li>Savyasachi (A) <a>NEW</a></li>
	                      <li>Taxiwala (UA) <a></a></li>
	                      <li>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></li>
	                      <li>Savyasachi (A) <a>NEW</a></li>
	                      <li>Taxiwala (UA) <a></a></li>
	                      <li>Savyasachi (A) <a>NEW</a></li>
	                    </ul>
	                  </div> 
	                </div>
	                <div className = "col-md-3">
	                  <div className = "movieLanguage"><ul> TAMIL</ul></div>
	                  <div className= "movies">
	                    <ul>
	                      <li>Amar Akbar Antony (UA) <a></a></li>
	                      <li>24 kisses (A)<a>NEW</a></li>
	                      <li>Sarkar (UA) <a></a></li>
	                      <li>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></li>
	                      <li>Taxiwala (UA) <a></a></li>
	                      <li>Taxiwala (UA) <a></a></li>
	                      <li>Sarkar (UA) <a></a></li>
	                      <li>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></li>
	                    </ul>
	                  </div> 
	                </div>
	                <div className = "col-md-3">
	                  <div className = "movieLanguage"><ul> ENGLISH</ul></div>
	                  <div className= "movies">
	                    <ul>
	                      <li>Amar Akbar Antony (UA) <a></a></li>
	                      <li>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></li>
	                      <li>Sarkar (UA) <a></a></li>
	                      <li>Savyasachi (A) <a>NEW</a></li>
	                      <li>Savyasachi (A) <a>NEW</a><a ><svg className = "svgHeart" xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 102 102" id="icon-heart" width="100%" height="100%"><path d="M4.3 39.4v-5.5c.9-3 1.4-6.2 2.8-9 4.4-8.9 11.6-14.2 22.1-14.2 8.2 0 14.1 4.1 18.9 10.2.6.8 1.2 1.6 1.8 2.5 1.6-2 3-3.9 4.6-5.5 6.1-6.1 13.5-8.7 22.2-6.5 12.4 3.1 19.9 15.4 18.8 28-.8 9.1-4.9 16.9-10.7 23.9-9.3 11.2-21.2 19.2-34 26.1-.4.2-1.3.2-1.8 0-11-5.9-21.3-12.7-29.9-21.6-5.9-6.1-10.8-12.9-13.2-21-.7-2.5-1-5-1.6-7.4z"></path></svg></a><p className="svgRating"><a></a>85%</p></li>
	                    </ul>
	                  </div> 
	                </div>
	              </div>
	          </div>
	        </div>
	    </div>
    )
  }
}

export default Quickbook;