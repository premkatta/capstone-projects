import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import './App.css';

import Home from './components/home';
import MoviesAll from './components/moviesAll';
import Quickbook from './components/quickbook';
import MovieDetailedInfo from './components/movieDetailedInfo';
import Theatres from './components/theatres';
import Seatbooking from './components/seatbooking';

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">   
          <Route path ="/" strict exact component={Home}/>
          <Route path = "/quickbook" strict exact component = {Quickbook}/>
          <Route path = "/movies" strict exact component= {MoviesAll}/>
          <Route path = "/movies/:movie_name" strict exact component ={MovieDetailedInfo}/>
          <Route path = "/movies/:movie_name/theatres/" exact component = {Theatres}/>
          <Route path ="/movies/:movie_name/:theatres/seatbooking" exact component = {Seatbooking}/>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
