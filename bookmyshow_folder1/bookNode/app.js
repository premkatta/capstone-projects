const express = require('express');
const bodyParser = require("body-parser");
const cors = require ('cors');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://premkatta:mongo1234@ds155614.mlab.com:55614/bms_database";
const serveStatic =  require('serve-static');
var path = require('path');

app.use(bodyParser());
app.use(cors());
app.use(serveStatic(__dirname+'/build'));

MongoClient.connect(url, function(err,client){
  app.post('/addBmsData', function (req,res){
    client.db("bms_database").collection("TeluguMovies").insertMany([req.body]).then(function(err, docs) {
      res.send({
        msg:"Done  "+  req.body.title
      })
    });
    // console.log(req.body);
  })
  app.get('/BmsData', function (req, res){
    client.db("bms_database").collection("TeluguMovies").find().toArray(function (err , docs){
      res.send(docs);
    });
  })

  app.get('/cinema/:name', function (req, res){
		// alert('working!');
    client.db("bms_database").collection("TeluguMovies").find({title:req.params.name}).toArray(function (err , docs){
      res.send(docs);
    });
  })

// app.get(':name/theatres/:theatre/',function (req, res){
// 	client.db("bms_database").collection("TheatresList").find({theatre_list:req.params.theatre}).toArray(function (err , docs){
// 	    res.send(docs);
// 	})
// })
  
  app.route('/*')
    .get(function (req, res) {
      res.sendFile(path.join(__dirname + '/build/index.html'));
    });
});

app.listen(process.env.PORT || 5000, ()=>{
  console.log("Running on port 5000");
});