import React from 'react';
import Homepage from '../components/Homepage';
import '../css/App.css';

function App() {
  return (
    <div className="App">
      <Homepage />
    </div>
  );
}

export default App;
