import React from 'react';
import '../css/Navbar.css';

export default function Navbar(){
	return(
		<div className='navbar-main'>
		<div className='container'>
			<div className='row'>
				<div className='col-lg-4'>Logo</div>
				<div className='col-lg-8'>
					<div className='row'>
						<div className='col-lg-2'>Home</div>
						<div className='col-lg-2'>About</div>
						<div className='col-lg-2'>Portfolio</div>
						<div className='col-lg-2'>Contact</div>
						<div className='col-lg-2 dropdown'>
							<button className='dropdown-toggle dropdown-button' data-toggle='dropdown'>
								Dropdown
							</button>
							<div className='dropdown-menu'>
								<a className='dropdown-item' href='#'>Projects</a>
								<a className='dropdown-item' href='#'>Services</a>
							</div>
						</div>
						<div className='col-lg-2'>Request</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	);
}