import React from 'react';
import Navbar from './Navbar';

class Homepage extends React.Component{
	render(){
		return(
			<div className='homepage-main'>
				<Navbar />
			</div>
		);
	}
}

export default Homepage;