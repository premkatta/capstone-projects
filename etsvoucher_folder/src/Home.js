import React , { Component } from 'react';
import './Home.css';

class Home extends React.Component{
    render(){
        return(
            <div className = "pagepanel">
             <div className = "pos-f-t mobileMenu">
                <nav class = "navbar navbar-dark bg-green">
                    <div className = "logoDiv">
                        <a>Varahi Solutions</a>
                    </div>
                    <button class = "navbar-toggler" type = "button" data-toggle ="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class ="navbar-toggler-icon"></span>
                    </button>
                </nav>
                <div className ="collapse" id="navbarToggleExternalContent">
                     <div class="bg-dark p-4">
                        <div class = "tabText" >
                            <p><a href="#offers">Courses offered</a></p>
                            <p><a href="#registration">Registration Form</a></p>
                            <p><a href="#contact-us">Contact US</a> </p>
                        </div>
                    </div>
                </div>
            </div>
                <div className = "navbarMain">
                    <div className = "logoDiv">
                        <a>Varahi Solutions</a>
                    </div>
                    <div className = "MenuOptions">
                        {/* <a>About</a> */}
                        <a href ="#offers">Courses Offered</a>
                        <a href ="#registration">Registration Form</a>
                        <a href = "#contact-us">Contact us</a>
                    </div>
                </div>
                <div className = "AboutData_gradient">
                <div className = "AboutData">
                    {/* <div className = "AboutData2">
                        <div className ="hola"><img src = {require("../src/images/hola.png")}/>Hola!</div>
                        <div className = "AboutData_info">Are you looking for writing  <a href="#Gre">GRE</a>,<a href="#Tofel">TOFEL</a >,<a href="#Gmat">GMAT</a>,<a href="#Ielts">IELTS</a>,<a href="#Pte">PTE</a> then you are at right place, we have discount vouchers for all.</div> 
                    </div> */}
                </div>
                </div>
               
                <div id ="offers" className = "offers">
                <div className = "offersTitle"><a>COURSES OFFERED</a></div>
                <div className = "offersPanel">
                <div className = "offersPic">
                    <img src = "https://silverfernrotorua.co.nz/wp-content/uploads/2014/05/Special-Offers-on-Rotorua-Accommodation.png"/>
                </div>
                <div class="row offers1">
                    <div class="col-sm-6">
                        <div id ="Gre" class="card">
                        <div class="card-body">
                            <h6 class="card-title">Save upto 30% on Python</h6>
                            <img src = {require("../src/images/python.png")} alt="Gre"/>
                            <div className ="limited">*Limited time offer</div>
                            <a href="#registration" class="btn btn-primary btn-sm">Get in touch with us</a>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id ="Tofel" class="card">
                        <div class="card-body">
                            <h6 class="card-title">Save upto 20% on PHP</h6>
                            <img src = {require("../src/images/php1.png")} alt="Tofel"/>
                            <div className ="limited">*Limited time offer</div>
                            <a href="#registration" class="btn btn-primary btn-sm">Get in touch with us</a>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="row offers2">
                    <div class="col-sm-6">
                        <div id ="Gmat" class="card">
                        <div class="card-body">
                            <h6 class="card-title">Save upto 15% on C++</h6>
                            <img src = {require("../src/images/C++.png")} alt="Gmat"/>
                            <div className ="limited">*Limited time offer</div>
                            <a href="#registration" class="btn btn-primary btn-sm">Get in touch with us</a>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id ="Ielts" class="card">
                        <div class="card-body">
                            <h6 class="card-title">Save upto 10% on Java</h6>
                            <img src = {require("../src/images/java.png")}/>
                            <div className ="limited">*Limited time offer</div>
                            <a href="#registration" class="btn btn-primary btn-sm">Get in touch with us</a>
                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id ="Pte" class="card">
                        <div class="card-body">
                            <h6 class="card-title">Save upto 10% on Js</h6>
                            <img src = {require("../src/images/js.png")} alt="pte"/>
                            <div className ="limited">*Limited time offer</div>
                            <a href="#registration" class="btn btn-primary btn-sm">Get in touch with us</a>
                        </div>
                        </div>
                    </div>
                    
                    </div>
                </div>
                </div>

                <div  id = "mockTest" className = "MockTestPanel">
                    <div className = "mockTest"><a>PREPARATION  MATERIALS</a></div>
                    <div class="row Mock_row">
                        <div class="col-sm-4">
                            <div id="GRE_Material" class="card Mock_card">
                            <div class="card-body">
                                <h6 class="card-title">Save upto 10% on Python Materials</h6>
                                <div class="gre_material_title">Php Material</div>
                                <div className ="limited">*Limited time offer</div>
                                <a href="#registration" class="btn btn-primary btn-sm">Get in touch with us</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div id="PTE_platinum" class="card Mock_card">
                            <div class="card-body">
                                <h6 class="card-title">Save upto 10% on Java Materials</h6>
                                <div class="PTE_platinum_title">Java Material</div>
                                <div className ="limited">*Limited time offer</div>
                                <a href="#registration" class="btn btn-primary btn-sm">Get in touch with us</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div  id = "refer" className = "ReferPanel">
                <div className = "offersTitle"><a>REFER & EARN</a></div>
                   <div className="refer_box">
                    <div className="refer_data">
                        <div>Refer your friends, to learn these technologies, from us.As a bonus you will be rewared with one the following,</div>
                        <div className="refer_items">
                            <div className="refer_item">
                                <img src={require("../src/images/Gpay.png")} alt="Gpay"/>
                                <div className="refer_item_data">Get Rs.200 Gpay Cash</div>
                            </div>
                            <div className="refer_item">
                                <img src={require("../src/images/phonepe.png")} alt="Phonpe"/>
                                <div className="refer_item_data">Get Rs.200 Phonepe Cash</div>
                            </div>
                            <div className="refer_item">
                                <img src={require("../src/images/paytm-wallet.jpg")} alt="paytm-wallet"/>
                                <div className="refer_item_data">Get Rs.200 Paytm Cash</div>
                            </div>
                        </div>
                        <a class="read_tc">*read t&c</a>
                    </div>
                    <div className="share">
                        <div>
                            <a href="whatsapp://send?text=Refer your friends and get a rewared of Rs.200 in your (Gpay/ Phonepe/ Paytm wallet), http://varahisolutions.herokuapp.com" data-action="share/whatsapp/share"><img className="whatsapp-img" src={require("../src/images/whatsapp-share-button.png")} alt="whatsapp_share"/></a>      
                            <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fvarahisolutions.herokuapp.com" target="_blank" rel="noopener" ><img className="fb-img" src={require("../src/images/fb-share-button.jpg")} alt="fb_share"/></a>                            
                            <p><a>*</a>Ask your Referee to enter your details(in referred by section), while filling the Regestration form below.Then only you can get the reward, upon successful registration.</p>
                        </div>      
                    </div>
                   </div>
                </div>

                <div  id = "registration" className = "RegistrationPanel">
                <div className = "Registration"><a>REGISTRATION</a></div>
                    <div className = "Reg-pic">
                    </div>
                    <div className = "Reg-form">
                    <form method ="POST" action = "https://formspree.io/varahisol1@gmail.com">
                        <p className = "communicate_tag">This is just used to communicate with you.</p>
                            <div>Please select your Course</div>
                            <div className = "preferrences">
                                <label className ="radio-inline">
                                    <input type ="checkbox" name ="optradio python"/>python
                                </label>
                                <label className ="radio-inline">
                                    <input type ="checkbox" name ="optradio php"/>php
                                </label>
                                <label className ="radio-inline">
                                    <input type ="checkbox" name ="optradio C++"/>C++
                                </label>
                                <label className ="radio-inline">
                                    <input type ="checkbox" name ="optradio Java"/>Java
                                </label>
                                <label className ="radio-inline">
                                    <input type ="checkbox" name ="optradio Js"/>Javascript
                                </label>
                                <label className ="radio-inline">
                                    <input type ="checkbox" name ="optradio Java_Material"/>Java Material
                                </label>
                                <label className ="radio-inline">
                                    <input type ="checkbox" name ="optradio Php_Material"/>Php Material
                                </label>
                            </div>
                          
                            <div className = "form-fill">
                                <div>Name<a>*</a> :</div>
                                    <input type ="text" name = "name" placeholder = "write your name" required></input>
                                <div>Email<a>*</a> :</div>
                                    <input type = "email" name ="email" placeholder = "write your valid email" required />
                                <div>Mobile<a>*</a> :</div>
                                    <input type = "text" name ="mobile" placeholder = "write your mobile number" required/>
                                <div>Referrd by :</div>
                                    <input type = "text" name ="referrd-by" placeholder = "write referred-by name & mobile number"/>
                                <div className ="submit"><button type ="button submit" class = "btn btn-primary btn-sm">submit</button></div>
                            </div>
                        </form>
                    </div>
                </div>
                <div className = "offersTitle"><a>CONTACT US</a></div>
                
                <div  id = "contact-us"className = "contactPanel">
                  <img className = "contact-us" src = {require("../src/images/contact-us.png")}/>
                    <div className = "contact">
                    <ul>
                        <li><span>
                            {/* <img src = {require("../src/images/phone.svg")}/> */}
                            <img src = {require("../src/images/whatsapp-logo.svg")}/>
                            <a href = "tel:+917416974393" >7416974393</a>
                        </span></li>
                        <li><div>
                            <img src ={require("../src/images/email.svg")}/>
                            <a href ="mailto:varahisol1@gmail.com">varahisol1@gmail.com</a>
                        </div></li>
                       <li> <div>
                            <img src ={require("../src/images/facebook.svg")}/>
                            <a href="https://fb.me/varahisolutions" target="_blank">varahisolutions</a>
                        </div></li>
                        <li><div>
                            <img src ={require("../src/images/placeholder.svg")}/>
                            <a>Hyderabad.</a>
                        </div></li>
                     </ul>
                    </div>
                </div>
                <div className ="footerPanel">
                    <p>Terms & conditions apply</p>
                    <a>Choose Career Wisely, Spend Money Wisely</a>
                    <a><img src={require("../src/images/pk_logo_triangle_color.png")} alt=""/></a>
                    <div className = "best">
                       <a>ALL THE BEST!!!</a>
                    </div>
                </div>
            </div>
        )
    };
}

export default Home;