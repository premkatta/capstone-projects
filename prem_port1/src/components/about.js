import React , {Component} from "react";

class About extends Component{
    constructor(props){
        super()
    }
     myFunction() {
        window.open("https://www.google.com");
    }

render(){
    return(
        <div class = "about">
        <div class = "photo">
            <img src = {require("../images/myPhoto.jpg")} alt="myPhoto"></img>
        </div>
         <div class = "Hi">
            <p>Hi</p>
         </div>
        <div class = "about_me">
            <p> I am <b> PREM KATTA </b>Full-stack Web Developer, develops custom web applications using front-end technologies like ReactJs, Redux and Bootstrap and back-end technologies like NodeJs, Express and MongoDB, and enthusiastic about the tech world.</p>
        </div>
        <div class = "Hire_me">
        <a href="mailto:premkatta121@gmail.com" target="_top"><button type="button" class="btn btn-primary">Hire Me</button></a>
      <a href = "https://drive.google.com/uc?export=download&id=1COO8SQ6ydQtKB12pD6PvkJ6gqBwmeXxv"> <button type="button" class="btn btn-primary Resume">Resume</button></a>
        </div>
        <div class = "About_contact">
            <span>
                <img class = "email2" src = {require("../images/email2.svg")} alt="email"></img>
                <a href="mailto:premkatta121@gmail.com" target="_top"><p>premkatta121@gmail.com</p></a>
            </span>
            <div class = "phoneDiv">
                <a href="tel:9676480178" target="blank"> <img class = "phone" src = {require("../images/phone.svg")} alt="phone"></img>
                <p>+91-9676480178</p> </a>
            </div>
            {/* <div class = "gitMenu">
                <img src = {require('../images/github_ipods.svg')}></img>
                <a href = "https://gitlab.com/dashboard/projects"><p class = "inline-block"> github</p></a>
            </div> */}
        </div>
            <a className="nameLogo"><img src={require("../images/pk_logo_triangle_color.png")} alt="nameLogo"/></a>
        </div>
    )

};

}

export default About;