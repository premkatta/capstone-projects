import React, {Component} from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import  {NavLink} from 'react-router-dom';


class Navicon extends Component{
    constructor(props){
        super()
    }
render(){
    return(
<div class="pos-f-t">
      <nav class="navbar navbar-dark bg-dark">
     
          <NavLink to = "/about"> <p class = " myname">- PREM KATTA -</p></NavLink>
      
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </nav> 
      <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-dark p-4">
          <div class = "tabText" >
            <NavLink to = "/about" class = " tabText tab_hOver" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation"> <p >ABOUT</p></NavLink>
            <NavLink to = "/skills"  class = "tabText tab_hOver" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation"> <p >SKILLS</p></NavLink>
            <NavLink to = "/projects" class = " tabText tab_hOver" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation"> <p >PROJECTS</p></NavLink>
            <NavLink to = "/contact" class = " tabText tab_hOver" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation"> <p >CONTACT</p></NavLink>       
          </div>
        </div>
      </div>
</div>
    )
};
}
export default Navicon;