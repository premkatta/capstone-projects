import React , {Component} from "react";


class Projects extends Component{
    constructor(props){
        super()
    }

render(){
    return(
        <div class = "projects">
        <p class = 'projectsTitle' >PROJECTS</p>
        <div class="card-group">
  <div class="card">
  <a target="_blank" href="https://myticketbooking.herokuapp.com"><img class="card-img-top laptop" src={require("../images/bms_home.png")} alt="Card image cap"></img></a>
    <div class="card-body">
      <h5 class="card-title">TICKET BOOKING SYSTEM</h5>
      <p class="card-text">By using this application, user can book tickets for the movies or any events.</p>
      <h5> DESCRIPTION</h5>
      <p class="card-text">This is a replica of Bookmyshow. User can select the movie and watch the info about the movie and choose the theatre and seats in it and proceed to book the ticket for it.</p>
      <h5>STACK TECHNOLOGIES</h5>
      <p class="card-text">HTML, CSS , ReactJS, Redux,  NodeJs, MongoDB.</p>
      <div class = "code_div">
        < img class = "code_logo" src ='https://camo.githubusercontent.com/7710b43d0476b6f6d4b4b2865e35c108f69991f3/68747470733a2f2f7777772e69636f6e66696e6465722e636f6d2f646174612f69636f6e732f6f637469636f6e732f313032342f6d61726b2d6769746875622d3235362e706e67'></img>
          <a target = "_blank" href = "https://gitlab.com/premkatta/capstone-projects/tree/master/bookmyshow_folder1"><p class = "inline-block code">VIEW CODE IN GIT</p></a> 
      </div>
    </div>
  </div>

  {/* <div class="card">
    <img class="card-img-top phone"src={require("../images/hotel-booking.jpg")}  alt="Card image cap"></img>
    <div class="card-body">
      <h5 class="card-title">ONLINE RESTAURENT WEBSITE</h5>
      <p class="card-text">This website will aggregate table bookings in a restaurent from multiple providers.</p>
      
      <h5> DESCRIPTION</h5>
       <p class="card-text">User data is stored in database (MongoDb), when an user registered through the form developed using (ReactJs,PassportJs) after approval from the managemnet.This data is useful to communicate to the users easily.</p>
      <p class="card-text">User can choose the restaurent with preferred table, based on the reviews and also get the rewards and discounts for booking and all these data will be stored in user activities. </p>
      <h5>STACK TECHNOLOGIES</h5>
      <p class="card-text">HTML,CSS,ReactJS,NodeJs and MongoDb.</p>

       <div class = "code_div">
        <img class = "code_logo" src ='https://camo.githubusercontent.com/7710b43d0476b6f6d4b4b2865e35c108f69991f3/68747470733a2f2f7777772e69636f6e66696e6465722e636f6d2f646174612f69636f6e732f6f637469636f6e732f313032342f6d61726b2d6769746875622d3235362e706e67'></img>
        <a href = "https://gitlab.com/premkatta/app1"><p class = "inline-block code" >VIEW CODE IN GITHUB</p> </a>
      </div>

    </div>
  </div> */}

  <div class="card">
    <a target="_blank" href="https://varahisolutions.herokuapp.com" ><img class="card-img-top laptop"src={require("../images/varahi-demo.png")}  alt="Abroad Exam Web image"></img> </a>
    <div class="card-body">
      <h5 class="card-title">VARAHI SOLUTIONS</h5>
      <p class="card-text">It is an online platform for students to enroll and get trained for the IT courses.</p>
      <h5> DESCRIPTION</h5>
      {/* <p class="card-text">User data is stored in database (MongoDb), when an user registered through the form developed using (ReactJs,PassportJs) after approval from the managemnet.This data is useful to communicate to the users easily.</p> */}
      <p class="card-text">
        This website gives the information about the courses offered. User can fill up the details and enroll for the courses offline, thorough the form displayed in the website and we we'll communicate to them.
      </p>
      <h5>STACK TECHNOLOGIES</h5>
      <p class="card-text">HTML,CSS and ReactJS</p>
      <div class = "code_div">
        <img class = "code_logo" src ='https://camo.githubusercontent.com/7710b43d0476b6f6d4b4b2865e35c108f69991f3/68747470733a2f2f7777772e69636f6e66696e6465722e636f6d2f646174612f69636f6e732f6f637469636f6e732f313032342f6d61726b2d6769746875622d3235362e706e67'></img>
        <a href = "https://gitlab.com/premkatta/"><p class = "inline-block code" >VIEW CODE IN GIT</p> </a>
      </div>
    </div>
  </div>
  </div>
  {/* <a className="nameLogo"><img src={require("../images/pk_logo_triangle_color.png")} alt="nameLogo"/></a> */}
</div>
    )

};

}

export default Projects;