import React , {Component} from "react";

class Skills extends Component{
    constructor(props){
        super()
    }

   
render(){
    return(
        <div class = "skills">
            <p class = "skillsTitle">SKILLS</p>
            <p>HTML</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '80%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>CSS3</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '69%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>JavaScript</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '72%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>ReactJs</p>

            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '70%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>Redux</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '66%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>NodeJS</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '43%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <p>MongoDb</p>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" style={{width: '38%'}}aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
            </div>

            <a className="nameLogo"><img src={require("../images/pk_logo_triangle_color.png")} alt="nameLogo"/></a>


        </div>
    )

};

}

export default Skills;