import React , {Component} from "react";

class Contact extends Component{
    constructor(props){
        super()
    }
  
   
render(){
    return(
        <div class = "contact">
        <form class = "gform" method = "POST" data-email="example@email.net" action ="https://script.google.com/macros/s/AKfycbxlsLpTJN878-x9tVyduHFxE06hdIjAHUjVzZUnlQ/exec"></form>
         <p class = 'contactTitle' >CONTACT</p>
        <form method="POST" action="https://formspree.io/premkatta121@gmail.com">
            <p>Name</p>
            <input type= "text"  name="name" class = "Line_input"></input>
            <br></br>
            <p>Email</p>
            <input type="email" name="email" class = "Line_input"></input>
            <br></br>
            <p>Message</p>
            <textarea  type ="text" name="message" class = "Line_input "></textarea>
            <br></br>
            <button type="button submit" class="btn btn-primary">Send Now</button>
        </form>
           <div class = "About_contact Contact_contact">
            <span>
                <img class = "email2" src = {require("../images/email2.svg")}></img>
                <a href="mailto:premkatta121@gmail.com" target="_top"> <p>premkatta121@gmail.com</p></a>
            </span>
            <div class = "phoneDiv">
                <img class = "phone" src = {require("../images/phone.svg")}></img>
                <a href="tel:9676480178" target="blank"> <p>+91-9676480178</p></a>
            </div>
        </div>
        </div>
    )

};

}

export default Contact;