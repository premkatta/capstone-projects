import React, { Component } from 'react';
import {Link,Switch,Route} from 'react-router-dom';
import Images from './images';
import Images1 from './images1';
import Images2 from './images2';
import Images3 from './images3';
import Images4 from './images4';
//import containerimage1 from './Containerimage1';
import Logos from './logos';
import Subpage from './Subpage';
import Subpage1 from './Subpage1';
import './App.css';
import './titledivs.css';
import './loginform.css';
import './search.css';
import './premium.css';
import './footer.css';
import axios from "axios";
var countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
class Header extends Component{
  
       componentDidMount() {
              {
                  function autocomplete(inp, arr) {
                      var currentFocus;
                      inp.addEventListener("input", function (e) {
                          var a, b, i, val = this.value;
                            closeAllLists();
                          if (!val) { return false; }
                          currentFocus = -1;
                            a = document.createElement("DIV");
                          a.setAttribute("id", this.id + "autocomplete-list");
                          a.setAttribute("class", "autocomplete-items");
                          this.parentNode.appendChild(a);
                          for (i = 0; i < arr.length; i++) {
                              if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                  b = document.createElement("DIV");
                                  b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                  b.innerHTML += arr[i].substr(val.length);
                                  b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                   b.addEventListener("click", function (e) {
                                     inp.value = this.getElementsByTagName("input")[0].value;
                                    closeAllLists();
                                  });
                                  a.appendChild(b);
                              }
                          }
                      });
                     inp.addEventListener("keydown", function (e) {
                          var x = document.getElementById(this.id + "autocomplete-list");
                          if (x) x = x.getElementsByTagName("div");
                          if (e.keyCode == 40) {
                             currentFocus++;
                             addActive(x);
                          } else if (e.keyCode == 38) { //up
                             currentFocus--;
                               addActive(x);
                          } else if (e.keyCode == 13) {
                              e.preventDefault();
                              if (currentFocus > -1) {
                                 if (x) x[currentFocus].click();
                              }
                          }
                      });
                      function addActive(x) {
                           if (!x) return false;
                          removeActive(x);
                          if (currentFocus >= x.length) currentFocus = 0;
                          if (currentFocus < 0) currentFocus = (x.length - 1);
                          x[currentFocus].classList.add("autocomplete-active");
                      }
                      function removeActive(x) {
                         for (var i = 0; i < x.length; i++) {
                              x[i].classList.remove("autocomplete-active");
                          }
                      }
                      function closeAllLists(elmnt) {
                         var x = document.getElementsByClassName("autocomplete-items");
                          for (var i = 0; i < x.length; i++) {
                              if (elmnt != x[i] && elmnt != inp) {
                                  x[i].parentNode.removeChild(x[i]);
                              }
                          }
                      }
                 document.addEventListener("click", function (e) {
                          closeAllLists(e.target);
                      });
                  }
       autocomplete(document.getElementById("myInput"), countries);
              }
          }
        //   form validation
        constructor() {
            super();
            this.state = {
              fields: {},
              errors: {}
              
            }
      
            this.handleChange = this.handleChange.bind(this);
            this.submituserRegistrationForm = this.submituserRegistrationForm.bind(this);
          };
          handleChange(e) {
            let fields = this.state.fields;
            fields[e.target.name] = e.target.value;
            this.setState({
              fields
            });
          }
          submituserRegistrationForm(e) {
            e.preventDefault();
            if (this.validateForm()) {
                let fields = {};    
                fields["emailid"] = "";
                fields["password"] = "";
                this.setState({fields:fields});
                alert("Form submitted");
            }
          }
          addData() {
            axios.post('http://localhost:5000/addData', {
             emailid: this.fields.state.emailid,
             password: this.fields.state.password
            }).then((res) => {
              console.log(res);
            })
          }  
          validateForm() {
            let fields = this.state.fields;
            let errors = {};
            let formIsValid = true;
            if (!fields["emailid"]) {
              formIsValid = false;
              errors["emailid"] = "Please enter your email-ID.";
            }
            if (typeof fields["emailid"] !== "undefined") {
              var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
              if (!pattern.test(fields["emailid"])) {
                formIsValid = false;
                errors["emailid"] = "Please enter valid email-ID.";
              }
            }
            if (!fields["password"]) {
              formIsValid = false;
              errors["password"] = "Please enter your password.";
            }
            if (typeof fields["password"] !== "undefined") {
              if (!fields["password"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
                 {/*.match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)*/}
                formIsValid = false;
                errors["password"] = "Please enter secure and strong password.";
              }
            }
            this.setState({
              errors: errors
            });
            return formIsValid;
          }
      //form validation ends here    
   
  render() {
    {
      var modal = document.getElementById('id01');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
    }
    return (
 <div className="App">
 <div>
 <div>

 <div className="body1">
 <div id="header">
 <div className="topnav" id="header">
 <nav id="topnav">
<div className="dropdown" id="dropdown1">
<button className="dropbtn" id="toggle">
<div className="bar1"></div>
<div className="bar2"></div>
<div className="bar3"></div>
<i className="fa "></i>
</button>
<Link className="Linkhover" to="/">
<p className="dropbtn"id="title">hotstar<i className="fa "></i> </p>
</Link>
<div className="dropdown-content1">
<div className="dropdown-content" >
<a href="#" id="active1"><i className="fa fa-home"></i>&emsp;HOME</a>
<a href="#"><i className="fa fa-television"></i>&emsp;TV</a>
<a href="#"><i className="fa fa-life-bouy"></i>&emsp;MOVIE</a>
<a href="#" ><i className="fa fa-life-ring"></i>&emsp;SPORTS</a>
<a href="#"><i className="fa fa-newspaper-o"></i>&emsp;NEWS</a>
<a href="#"><i className="fas fa-star"></i>&emsp;PREMIUM</a>
<a href="#" ><i className="fa fa-television"></i>&emsp;CHANNELS</a>
<div className="dropdown-contentlanguage"><a href="#" ><i className="fa fa-language"></i>&emsp;LANGUAGES</a></div>
<a href="#"><i className="fas fa-gift"></i>&emsp;GENRES</a>
</div> </div>
</div> 

<a href="#"><i className="fa fa-language"></i>&emsp;LANGUAGES</a>
<a href="#"><i className="fas fa-gift"></i>&emsp;GENRES</a>
</nav> </div>
</div> 
<div className="dropdown" id="dropdown1">
<button className="dropbtn" >
TV
<i className="fa "></i>
</button>
<div className="dropdown-content">
<div className="dropdown-content" >
<a href="#">StarPlus</a>
<a href="#">Star jalsha</a>
<a href="#">Star Vijay</a>
<a href="#">Star Bharat</a>
<a href="#">Life Ok</a>
<a href="#">Asianet</a>
<a href="#">Star Maa</a>
<a href="#">Star World</a>
<a href="#">More...</a>
</div> </div>
</div> 
<div className="dropdown" id="dropdown1">
<button className="dropbtn" id="dropbtn1" >MOVIES
<i className="fa "></i>
</button>
<div className="dropdown-content">
<div className="dropdown-content" >
<a href="#">Hindi</a>
<a href="#">Bengali</a>
<a href="#">Telugu</a>
<a href="#">Malayalam</a>
<a href="#">Tamil</a>
<a href="#">Marathi</a>
<a href="#">English</a>
<a href="#">Kannada</a>
<a href="#">Gujarati</a>
</div> </div>
</div> 
<div className="dropdown" id="dropdown1">
<button className="dropbtn" id="dropbtn2">SPORTS
<i className="fa "></i>
</button>
<div className="dropdown-content">
<div className="dropdown-content" >
<a href="#">Kabaddi</a>
<a href="#">Football</a>
<a href="#">Badminton</a>
<a href="#">Hockey</a>
<a href="#">Tennis</a>
<a href="#">Formula 1</a>
<a href="#">Formula 2 </a>
<a href="#">Table Tennis</a>
<a href="#">Athletics</a>
<a href="#">Golf</a>
<a href="#">Swimming</a>
<a href="#">eSports</a>
<a href="#">Boxing</a>
</div> </div>
</div> 
<div className="dropdown">
<button className="dropbtn" id="dropdownspace4">NEWS
<i className="fa "></i>
</button></div>
<div className="dropdown">
<button className="dropbtn"id="dropdownspace5">PREMIUM
<i className="fa "></i>
</button></div>
<div className="dropdown">
<button className="dropbtn" id="dropdownspace6"><p className="searchhidden">SEARCH</p>
<div id="wrap">
  <form action="/action_page.php" autoComplete="off">
  <input id="myInput" name="myCountry" className="search"  name="search" type="text" placeholder=""/>
  </form>
</div>
</button></div>
{/*form starts here*/}
<div id="main-registration-container">
     <div id="register">
<button className="dropbtn" id="dropdownspace6">SEARCH
</button></div>
<div className="dropdown">
<button onClick={()=>document.getElementById('id01').style.display='block'} width="auto" className="dropbtn" id="dropdownspace7">SIGN IN </button>
<div id="id01" className="modal">
<form className="modal-content animate" id ="loginform1" action="/action_page.php">
<div className="imgcontainer">
<span onClick={() => document.getElementById('id01').style.display = 'none'} class="close" title="Close Modal">&times;</span>
<p className="signin1"  >Sign In</p>
</div>

<div className="facebooklogin"><div className="fb-wrap">
<a class="fb" href="https://www.facebook.com/hotstar" target="_blank"><img className="fbicon" src ="https://www.freeiconspng.com/uploads/facebook-f-logo-white-background-21.jpg" alt="img"/><span className="facebooktext" id="fbtxt">SIGN IN WITH FACEBOOK</span></a>
{/* <img src="img_avatar2.png" alt="Avatar" class="avatar"></img> */}
<p className="signin1"  >Sign In</p>
</div>
<div className="facebooklogin"><div className="fb-wrap">
<img className="fbicon" src ="https://www.freeiconspng.com/uploads/facebook-f-logo-white-background-21.jpg" alt="img"/><span className="facebooktext" id="fbtxt">SIGN IN WITH FACEBOOK</span>
</div></div>
<div className="divhr1"><hr className="hrwidth1"/><p className="por">OR</p>
<hr className="hrwidth2"/></div>
<div className="inputtype">
<input type="emailid" className="email" id="email1"name="emailid" value={this.state.fields.emailid} onChange={this.handleChange}  placeholder="Enter Email"/>
<div className="errorMsg">{this.state.errors.emailid}</div>
<form name ="myform" onsubmit="return validation();"/> 
     <input type="password" className="email" id="email2"value={this.state.fields.password} onChange={this.handleChange} name="password" placeholder="Enter Password" ></input>
      <div className="errorMsg">{this.state.errors.password}</div>
<div className="facebooklogin1">
<span className="logintext" id="fbtxt" value={this.state.fields.Register} onClick={this.submituserRegistrationForm}  > SIGN IN</span>
</div>
<div className="forgotpwd">FORGOT PASSWORD</div>
</div>
</div>
</form>
</div></div></div>
<div className="inputtype"><input type="email" className="email" id="email1"name="Email" placeholder="Enter Email"/>
<input type="password" className="email" id="email2" name="pwd" placeholder="Enter Password" ></input>
<div className="facebooklogin1">
<span className="logintext" id="fbtxt">SIGN IN</span>
</div>
<div className="forgotpwd">Forgot Password</div>
</div>
</div>
</div>
<div className="slideropen">
<div id="myCarousel" className="carousel slide" data-ride="carousel">
<ol className="carousel-indicators">
<li data-target="#myCarousel" data-slide-to="0" className="active"></li>
<li data-target="#myCarousel" data-slide-to="1"></li>
<li data-target="#myCarousel" data-slide-to="2"></li>
<li data-target="#myCarousel" data-slide-to="3"></li>
<li data-target="#myCarousel" data-slide-to="4"></li>
<li data-target="#myCarousel" data-slide-to="5"></li>
</ol>
<div className="carousel-inner">
<div className="item active">
<iframe width="100%" height="320" src="https://www.youtube.com/embed/QYMsbv5h2gQ" ></iframe>
</div>
<div className="item">
<iframe width="100%" height="320" src="https://www.youtube.com/embed/gzzpfxN42oM"  ></iframe>
</div>
<div className="item">
<iframe width="100%" height="320" src="https://www.youtube.com/embed/f5tfueNj3Zs" ></iframe>
</div>
<div className="item">
<iframe width="100%" height="320" src="https://www.youtube.com/embed/TaBH78dEWsw"  ></iframe>
</div>
<div className="item">
<iframe width="100%" height="320" src="https://www.youtube.com/embed/HtSimH3vtgU"></iframe>
 </div>

<div className="item">
<iframe width="100%" height="320" src="https://www.youtube.com/embed/fR4yJYfZ_aA" ></iframe>
</div>
</div>
<a className="left carousel-control" href="#myCarousel" data-slide="prev">
<span className="glyphicon glyphicon-chevron-left"></span>
<span className="sr-only">Previous</span>
</a>
<a className="right carousel-control" href="#myCarousel" data-slide="next">
<span className="glyphicon glyphicon-chevron-right"></span>
<span className="sr-only">Next</span>
</a>
</div></div>
<div className="heading1">
<h3 className="containertitle">Top Picks For You&nbsp;
<i className="fa fa-angle-right"id="arrow1"></i>
</h3>
</div>
<div className="containerdiv1">
  
       <Images />
      {/* <Logos /> */}
        <div></div></div>
        <div className="heading1">
<h3 className="containertitle">Popular Tv Shows&nbsp;
<i className="fa fa-angle-right"id="arrow1"></i>
</h3>
</div>
<div className="containerdiv1">
       <Images1 />
        </div>
        <div className="heading1">
<h3 className="containertitle">Popular Movies&nbsp;
<i className="fa fa-angle-right"id="arrow1"></i>
</h3>
</div>
<div className="containerdiv1">
       <Images2 />
        </div>
        <div className="heading1">
<h3 className="containertitle">Live News&nbsp;
<i className="fa fa-angle-right"id="arrow1"></i>
</h3>
</div>
<div className="containerdiv1">
       <Images3 />
        </div>
        <div className="heading1">
        <h3 className="containertitle">New On Hotstar&nbsp;
        <i className="fa fa-angle-right"id="arrow1"></i>
        </h3>
        </div>
        <div className="containerdiv1">
               <Images />
               </div>
               <div className="heading1">
               <h3 className="containertitle">Best in Sports&nbsp;
               <i className="fa fa-angle-right"id="arrow1"></i>
               </h3>
               </div>
               <div className="containerdiv1news">
                      <Images4 />
                      </div>
                      <div className="heading1">
                      <h3 className="containertitle">Popular In Drama&nbsp;
                      <i className="fa fa-angle-right"id="arrow1"></i>
                      </h3>
                      </div>
                      <div className="containerdiv1">
                             <Images/>
                              </div>
                      
        <div className="footer">
        <span className="footerspan">
        <span className="footertxt"><a className="footertxta" href="#">About Hotstar</a></span>
        <span className="footertxt"><a className="footertxta" href="#">Terms Of Use</a></span>
        <span className="footertxt"><a className="footertxta" href="#"> Privacy Policy</a></span>
        <span className="footertxt"><a className="footertxta" href="#">FAQ</a></span>
        <span className="footertxt"><a className="footertxta" href="#">Feedback</a></span>
        <span className="footertxt"><a className="footertxta" href="#">Careers</a></span>
        <span className="footertxt1"><a className="footertxta1"id="footertxtspace" href="#"><b className="b1"> Connect With us </b></a>
        </span>
        <span className="footertxt1"><a className="footertxta1" id="footertxtspace1" href="#"><b className="b1">Hotstar App</b></a>
        </span>
        <p className="footerpara">© 2018 STAR. All Rights Reserved. HBO, Home Box Office and all related channel and programming logos are service marks of, and all related programming visuals and elements are the property of, Home Box Office, Inc. All rights reserved.</p>
        </span>
        <a class="appstore" href="https://itunes.apple.com/in/app/hotstar/id934459219?mt=8" target="_blank">
        <span className="fab fa-facebook-f"id="footerfacebook" font-Size="48px">
        </span></a>
        <a class="tw" href="https://twitter.com/hotstartweets" target="_blank">
        <span className="fa fa-twitter"id="footertwitter" font-Size="48px"></span></a>
        <a class="playstore" href="https://play.google.com/store/apps/details?id=in.startv.hotstar" target="_blank">
        <div className="footerplaystorebox">
        <span className="footerplaystore">
        <p className="footerplaystoretxt">get it on</p>
        <p className="footerplaystoretxt1">Google Play</p>
        <img className="footerplaystore1"  src="http://www.rabsnetsolutions.com/wp-content/uploads/2017/05/playstore.png"alt="img"/>
        <a className="playstore" href="https://play.google.com/store/apps/details?id=in.startv.hotstar" target="_blank" ></a>
        </span></div></a>
        <a class="appstore" href="https://itunes.apple.com/in/app/hotstar/id934459219?mt=8" target="_blank">
        <div className="footerappstorebox">
        <span className="footerappstore">
        <p className="footerappstoretxt">Download on the</p>
        <p className="footerappstoretxt1">App Store</p>
        <img className="footerappstore1"  src="http://www.p2p2p2.com/img173/mfrvpqafjfyshlltstma.png"alt="img"/>
        <a className="playstore" href="https://play.google.com/store/apps/details?id=in.startv.hotstar" target="_blank" data-reactid="274"></a>
        </span></div></a>
       <span className="fab fa-facebook-f"id="footerfacebook" font-Size="48px">
       </span>
       <span className="fa fa-twitter"id="footertwitter" font-Size="48px"></span>
       <div className="footerplaystorebox">
       <span className="footerplaystore">
       <p className="footerplaystoretxt">get it on</p>
       <p className="footerplaystoretxt1">Google Play</p>
       <img className="footerplaystore1"  src="http://www.rabsnetsolutions.com/wp-content/uploads/2017/05/playstore.png"alt="img"/>
       <a className="playstore" href="https://play.google.com/store/apps/details?id=in.startv.hotstar" target="_blank" ></a>
       </span></div>
       <div className="footerappstorebox">
       <span className="footerappstore">
       <p className="footerappstoretxt">Download on the</p>
       <p className="footerappstoretxt1">App Store</p>
       <img className="footerappstore1"  src="http://www.p2p2p2.com/img173/mfrvpqafjfyshlltstma.png"alt="img"/>
       <a className="playstore" href="https://play.google.com/store/apps/details?id=in.startv.hotstar" target="_blank" data-reactid="274"></a>
       </span></div>
        </div>  
        </div>
        </div>  
    );
  }
};
export default Header;
  