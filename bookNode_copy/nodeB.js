const express = require('express');
const bodyParser = require("body-parser");
const cors = require ('cors');
const app = express();
const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017";

app.use(bodyParser());
app.use(cors());

MongoClient.connect(url, function(err,client){
    app.post('/addBmsData', function (req,res){
       
        client.db("Bookmyshow").collection("Telugu Movies").insertMany([req.body]).then(function(err, docs) {
            res.send({
                msg:"Done  "+  req.body.title
            })
        });
        // console.log(req.body);
    })
    app.get('/BmsData', function (req, res){
        client.db("Bookmyshow").collection("Telugu Movies").find().toArray(function (err , docs){
            res.send(docs);
        });
    })


});

app.listen(3031, ()=>{
    console.log("Running on port 3031");
});